package com.example.loki;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class UserFragment extends Fragment {

    View view;
    SharedPreferences prefs_user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view = inflater.inflate(R.layout.fragment_user, container, false);

        data();

        return view;
    }

    private void data(){
        prefs_user = getContext().getSharedPreferences("preferencias_usuario", Context.MODE_PRIVATE);
        Picasso.with(getContext()).load(prefs_user.getString("foto", "")).into((ImageView) view.findViewById(R.id.foto_perfil));

        TextView textView = (TextView) view.findViewById(R.id.nombre_);
        textView.setText(prefs_user.getString("name", ""));

        TextView textView1 = (TextView) view.findViewById(R.id.email_);
        textView1.setText(prefs_user.getString("email", ""));
    }
}