package com.example.loki;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MateriasFragment extends Fragment {

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_materias, container, false);

        user();
        database();

        if (Build.VERSION.SDK_INT > 16) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        view.findViewById(R.id.card_matematicas).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intrucciones(1);
            }
        });

        view.findViewById(R.id.card_ciencias).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intrucciones(2);
            }
        });

        view.findViewById(R.id.card_historia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intrucciones(3);
            }
        });

        view.findViewById(R.id.card_gramatica).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "No disponible", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    private void intrucciones(final int materia){
        Fragment fragment = new InstruccionesFragment(materia);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void user(){
        ImageView imageView = view.findViewById(R.id.user);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new UserFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    private void database(){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getContext(), "database_progress", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        Cursor c = BaseDeDatos.rawQuery("SELECT * FROM progress",null);


        if (c != null) {

            c.moveToFirst();

            do {
//                Toast.makeText(getContext(), c.getInt(0)+","+c.getInt(1), Toast.LENGTH_SHORT).show();
                TextView textView;
                switch (c.getInt(0)){

                    case 1: textView = view.findViewById(R.id.resultado_1);
                        textView.setText(c.getString(1));
                        break;

                    case 2: textView = view.findViewById(R.id.resultado_2);
                        textView.setText(c.getString(1));
                        break;

                    case 3: textView = view.findViewById(R.id.resultado_3);
                        textView.setText(c.getString(1));
                        break;
                }
            }
            while (c.moveToNext());
        }

        BaseDeDatos.close();
        c.close();
    }


}