package com.example.loki;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Timer;
import java.util.TimerTask;

public class PreguntaFragment extends Fragment {

    private View view;
    private RadioButton selected_ = null;
    private LinearLayout selected = null;
    private TextView textView;
    private Timer timer = new Timer();
    private int seg;

    private int respuesta_correcta = 0;
    private int respuesta_seleccionada = -1;
    private int pregunta = 1;
    int resultado = 0;

    //

    int materia;

    public PreguntaFragment(int materia) {
        this.materia = materia;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pregunta, container, false);
        select_reply_1();
        select_reply_2();
        select_reply_3();
        textView = view.findViewById(R.id.count);
        count_();

        iniciar_materia();
        next();

        return view;
    }

    private void iniciar_materia(){
        switch (materia){
            case 1:
                preguntas_matematicas(pregunta);
                break;
            case 2:
                preguntas_ciencia(pregunta);
                break;
            case 3:
                preguntas_historia(pregunta);
                break;
        }
    }

    private void select_reply_1(){
        final LinearLayout linearLayout = view.findViewById(R.id.content_reply_1);
        final RadioButton radioButton = view.findViewById(R.id.reply_1);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected_ != null){
                    selected_.setChecked(false);
                }
                selected_ = radioButton;
                radioButton.setChecked(true);
                if(selected != null){
                    selected.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_gray));
                }
                linearLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_primary));
                selected = linearLayout;

                respuesta_seleccionada = 1;
            }
        });
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected_ != null){
                    selected_.setChecked(false);
                }
                selected_ = radioButton;
                radioButton.setChecked(true);
                if(selected != null){
                    selected.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_gray));
                }
                linearLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_primary));
                selected = linearLayout;

                respuesta_seleccionada = 1;
            }
        });
    }

    private void select_reply_2(){
        final LinearLayout linearLayout = view.findViewById(R.id.content_reply_2);
        final RadioButton radioButton = view.findViewById(R.id.reply_2);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected_ != null){
                    selected_.setChecked(false);
                }
                selected_ = radioButton;
                radioButton.setChecked(true);
                if(selected != null){
                    selected.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_gray));
                }
                linearLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_primary));
                selected = linearLayout;

                respuesta_seleccionada = 2;
            }
        });
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected_ != null){
                    selected_.setChecked(false);
                }
                selected_ = radioButton;
                radioButton.setChecked(true);
                if(selected != null){
                    selected.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_gray));
                }
                linearLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_primary));
                selected = linearLayout;

                respuesta_seleccionada = 2;
            }
        });
    }

    private void select_reply_3(){
        final LinearLayout linearLayout = view.findViewById(R.id.content_reply_3);
        final RadioButton radioButton = view.findViewById(R.id.reply_3);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected_ != null){
                    selected_.setChecked(false);
                }
                selected_ = radioButton;
                radioButton.setChecked(true);
                if(selected != null){
                    selected.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_gray));
                }
                linearLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_primary));
                selected = linearLayout;

                respuesta_seleccionada = 3;
            }
        });
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected_ != null){
                    selected_.setChecked(false);
                }
                selected_ = radioButton;
                radioButton.setChecked(true);
                if(selected != null){
                    selected.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_gray));
                }
                linearLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_primary));
                selected = linearLayout;

                respuesta_seleccionada = 3;
            }
        });
    }

    private void select_reset(){

        if(selected_ != null){
            selected_.setChecked(false);
            selected.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_gray));
        }

    }

    class count extends TimerTask{
        @Override
        public void run() {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(seg+"");
                    seg--;
                    if(seg == 0){
                        seg = 15;

                        evaluar_respuesta();
                    }
                }
            });
        }
    }

    public void count_()
    {
        this.seg=15;
        timer = new Timer();
        timer.schedule(new count(), 0, 1000);
    }

    private void next(){
        LinearLayout linearLayout = view.findViewById(R.id.siguiente_pregunta);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluar_respuesta();
            }
        });
    }

    private void evaluar_respuesta(){
        if(respuesta_seleccionada == respuesta_correcta){
            Toast.makeText(getContext(), "Respuesta Correcta", Toast.LENGTH_SHORT).show();
            resultado++;
        }
        else{
            Toast.makeText(getContext(), "Respuesta Incorrecta", Toast.LENGTH_SHORT).show();
        }

        respuesta_correcta = 0;
        respuesta_seleccionada = -1;
        pregunta++;

        iniciar_materia();

        timer.cancel();
        if(pregunta != 6){
            count_();
        }

        select_reset();
        pintar_contador();
    }

    private void pintar_contador(){
        switch (pregunta){
            case 2:
                view.findViewById(R.id.contador_2).setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_primary));
                break;
            case 3:
                view.findViewById(R.id.contador_3).setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_primary));
                break;
            case 4:
                view.findViewById(R.id.contador_4).setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_primary));
                break;
            case 5:
                view.findViewById(R.id.contador_5).setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_input_primary));
                break;
        }
    }

    private void preguntas_ciencia(int pregunta){

        TextView textView = view.findViewById(R.id.txt_pregunta);
        TextView textView1 = view.findViewById(R.id.respuesta_1);
        TextView textView2 = view.findViewById(R.id.respuesta_2);
        TextView textView3 = view.findViewById(R.id.respuesta_3);

        ImageView imageView = view.findViewById(R.id.image_pregunta);
        imageView.setVisibility(View.GONE);

        switch (pregunta){
            case 1:
                textView.setText("Co2");
                textView1.setText("Agua");
                textView2.setText("Dióxido de Carbono");
                textView3.setText("Ácido clorhídrico");
                respuesta_correcta = 2;
                break;

            case 2:
                textView.setText("El sol es:");
                textView1.setText("Un Satélite");
                textView2.setText("Un planeta");
                textView3.setText("Una estrella");
                respuesta_correcta = 3;
                break;

            case 3:
                textView.setText("¿Qué estudia la ficología?");
                textView1.setText("Las algas");
                textView2.setText("Los hongos");
                textView3.setText("Seres vivos");
                respuesta_correcta = 1;
                break;

            case 4:
                textView.setText("¿A que velocidad viaja la luz?");
                textView1.setText("300,000 m/s");
                textView2.setText("300,00 Km/s");
                textView3.setText("300,000 Km/h");
                respuesta_correcta = 2;
                break;

            case 5:
                textView.setText("¿Quién es?");

                imageView.setImageResource(R.drawable.bacon_francis);
                imageView.setVisibility(View.VISIBLE);
                textView1.setText("Galilelo Galilei");
                textView2.setText("Francis Bacon");
                textView3.setText("René Descartes");
                respuesta_correcta = 2;
                break;

            case 6:
                Fragment fragment = new ResultadoFragment(resultado,materia);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content, fragment);
                fragmentTransaction.commit();
                break;

        }

    }

    private void preguntas_historia(int pregunta){

        TextView textView = view.findViewById(R.id.txt_pregunta);
        TextView textView1 = view.findViewById(R.id.respuesta_1);
        TextView textView2 = view.findViewById(R.id.respuesta_2);
        TextView textView3 = view.findViewById(R.id.respuesta_3);

        ImageView imageView = view.findViewById(R.id.image_pregunta);
        imageView.setVisibility(View.GONE);

        switch (pregunta){
            case 1:
                textView.setText("Primer poblador del Perú:");
                textView1.setText("Lauricocha");
                textView2.setText("Guitarrero");
                textView3.setText("Pacaicasa");
                respuesta_correcta = 3;
                break;

            case 2:
                textView.setText("Inca que nació en Tamboquiro y que puso el nombre al cusco:");
                textView1.setText("Sinchi Roca");
                textView2.setText("Mayta Cápac");
                textView3.setText("Huayna Cápac");
                respuesta_correcta = 1;
                break;

            case 3:
                textView.setText("¿Qué significa CHASQUI?");
                textView1.setText("Correo");
                textView2.setText("El que toma una cosa y la transmite");
                textView3.setText("Corredor");
                respuesta_correcta = 1;
                break;

            case 4:
                textView.setText("Fecha en que se fundó Lima:");
                textView1.setText("16 enero 1535");
                textView2.setText("18 enero 1535");
                textView3.setText("15 enero 1586");
                respuesta_correcta = 2;
                break;

            case 5:
                textView.setText("¿Quién es este personaje?");

                imageView.setImageResource(R.drawable.bleguia);
                imageView.setVisibility(View.VISIBLE);
                textView1.setText("Manuel Odría");
                textView2.setText("Augusto Bernardino Leguía y Salcedo");
                textView3.setText("Ricardo Pérez Godoy");
                respuesta_correcta = 2;
                break;

            case 6:
                Fragment fragment = new ResultadoFragment(resultado,materia);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content, fragment);
                fragmentTransaction.commit();
                break;

        }

    }

    private void preguntas_matematicas(int pregunta){

        TextView textView = view.findViewById(R.id.txt_pregunta);
        TextView textView1 = view.findViewById(R.id.respuesta_1);
        TextView textView2 = view.findViewById(R.id.respuesta_2);
        TextView textView3 = view.findViewById(R.id.respuesta_3);

        ImageView imageView = view.findViewById(R.id.image_pregunta);
        imageView.setVisibility(View.GONE);

        switch (pregunta){
            case 1:
                textView.setText("3 + 3 x 3 + 3 =");
                textView1.setText("21");
                textView2.setText("36");
                textView3.setText("15");
                respuesta_correcta = 3;
                break;

            case 2:
                textView.setText("¿Cuánto es la cuarta parte de la tercera parte?");
                textView1.setText("3/4");
                textView2.setText("1/12");
                textView3.setText("1/7");
                respuesta_correcta = 2;
                break;

            case 3:
                textView.setText("Si 10 + x es 5 más que 10, ¿Cuál es el valor de 2x?");
                textView1.setText("5");
                textView2.setText("-5");
                textView3.setText("10");
                respuesta_correcta = 3;
                break;

            case 4:
                textView.setText("Una finca tiene 120 hectáreas. Si se siembran las dos terceras partes, ¿Qué parte queda sin sembrar?");
                textView1.setText("80 hectáreas");
                textView2.setText("60 hectáreas");
                textView3.setText("40 hectáreas");
                respuesta_correcta = 3;
                break;

            case 5:
                textView.setText("¿Quién es?");

                imageView.setImageResource(R.drawable.galileo_galilei);
                imageView.setVisibility(View.VISIBLE);
                textView1.setText("Galilelo Galilei");
                textView2.setText("George Berkeley");
                textView3.setText("René Descartes");
                respuesta_correcta = 1;
                break;

            case 6:
                Fragment fragment = new ResultadoFragment(resultado,materia);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content, fragment);
                fragmentTransaction.commit();
                break;

        }

    }

    @Override
    public void onStop() {
        super.onStop();
        timer.cancel();
    }
}