package com.example.loki;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.concurrent.Executor;

public class LoginFragment extends Fragment {

    private View view;
    private VideoView videoBG;
    private MediaPlayer mMediaPlayer;
    private int mCurrentVideoPosition;

    ////

    private GoogleApiClient mGoogleApiClient;
    public static final int RC_SIGN_IN=9001;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;


    //

    SharedPreferences prefs_user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_login, container, false);

        main();

        return view;
    }

    public void main(){
        prefs_user = getContext().getSharedPreferences("preferencias_usuario", Context.MODE_PRIVATE);
        String s = prefs_user.getString("email", "");
        if(s.equals("")){
            star_video();
            login();
            google();
        }
        else next();
    }

    private void star_video(){
        if (Build.VERSION.SDK_INT > 16) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        videoBG = view.findViewById(R.id.videoView);
        Uri uri = Uri.parse("android.resource://"+ getActivity().getPackageName()+ "/"+ R.raw.food);
        videoBG.setVideoURI(uri);
        videoBG.start();
        videoBG.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mMediaPlayer = mediaPlayer;
                mMediaPlayer.setLooping(true);
                if (mCurrentVideoPosition != 0) {
                    mMediaPlayer.seekTo(mCurrentVideoPosition);
                    mMediaPlayer.start();
                }
            }
        });
    }

    private void login(){
        Button btn = view.findViewById(R.id.btn_login);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
    }

    private void next(){
            Fragment fragment = new MateriasFragment();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content, fragment);
//            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
    }

    @Override
    public void onPause() {
        super.onPause();
        mCurrentVideoPosition = mMediaPlayer.getCurrentPosition();
        videoBG.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        videoBG.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        catch (Exception e){}
    }


    public void google(){

        GoogleSignInOptions gso=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("511829128795-ee1q5fdc6okq3echqkvsln24d0tid8ho.apps.googleusercontent.com")
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
//                .enableAutoManage(getActivity(),)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        SignInButton btnGoogle = view.findViewById(R.id.btn_google);

        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(i,RC_SIGN_IN);
            }
        });

        firebaseAuth= FirebaseAuth.getInstance();
        firebaseAuthListener=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser usuario=firebaseAuth.getCurrentUser();
                if (usuario!=null)
                {
//                    System.out.println(usuario.getEmail());
//                    Toast.makeText(getContext(), "Se pudo acceder", Toast.LENGTH_SHORT).show();
//                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
//                    finish();

                }
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Toast.makeText(getActivity(), ""+requestCode, Toast.LENGTH_SHORT).show();
        if (requestCode==RC_SIGN_IN)
        {
            GoogleSignInResult result=Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess())
            {
                AutenticarFirebase(result.getSignInAccount());
                next();

                SharedPreferences.Editor editor = prefs_user.edit();
                editor.putString("email", result.getSignInAccount().getEmail());
                editor.putString("cuenta", result.getSignInAccount().getAccount()+"");
                editor.putString("name", result.getSignInAccount().getGivenName());
                editor.putString("foto", result.getSignInAccount().getPhotoUrl()+"");
                editor.apply();

            }else
            {
                Toast.makeText(getActivity(), result.getStatus()+"", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void AutenticarFirebase(GoogleSignInAccount signInAccount) {
        AuthCredential credencial= GoogleAuthProvider.getCredential(signInAccount.getIdToken(),null);
        firebaseAuth.signInWithCredential(credencial).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful())
                {
                    Toast.makeText(getActivity().getApplicationContext(), "", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (firebaseAuthListener!=null)
        {
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }
}