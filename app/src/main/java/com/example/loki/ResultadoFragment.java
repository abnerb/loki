package com.example.loki;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ResultadoFragment extends Fragment {

    private View view;
    int resultado;
    int materia;

    public ResultadoFragment(int resultado, int materia) {
        this.resultado = resultado;
        this.materia = materia;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_resultado, container, false);
        mostrar_resultado();
        next();
        guardar_progreso();
        return view;
    }

    private void mostrar_resultado(){
        TextView textView = view.findViewById(R.id.txt_resultado);
        textView.setText(resultado+"");
    }

    private void guardar_progreso(){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getActivity().getApplicationContext(), "database_progress", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();

        BaseDeDatos.rawQuery("DELETE FROM progress WHERE materia = "+materia,null);

        ContentValues registro = new ContentValues();

        registro.put("materia", materia);
        registro.put("puntaje", resultado);

        BaseDeDatos.insert("progress", null, registro);
        BaseDeDatos.close();
    }

    private void next(){
        LinearLayout linearLayout = view.findViewById(R.id.next_resultado);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getActivity().onBackPressed();
                }
                catch (Exception e){}
            }
        });
    }
}